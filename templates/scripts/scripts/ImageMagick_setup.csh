#!/bin/tcsh -f

set MAGICK_HOME=/nrgpackages/tools/ImageMagick
set path=($MAGICK_HOME/bin ${path})
#set LD_LIBRARY_PATH=$MAGICK_HOME/lib ${LD_LIBRARY_PATH}
if ($?LD_LIBRARY_PATH) then
  setenv LD_LIBRARY_PATH $MAGICK_HOME/lib:${LD_LIBRARY_PATH}
else
  setenv LD_LIBRARY_PATH $MAGICK_HOME/lib
endif

setenv MAGICK_HOME $MAGICK_HOME
